<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smspanel
 */

?>
<div class="clearfix"></div>
       <div class="footer">
           <div class="container-fluid">
           <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
               <div class="col-md-3 col-sm-3 " >
                   <div class="about-us"><?php smspanel_option('aboutustitile'); ?> </div>
                   <p class="about-us-content"><?php smspanel_option('aboutus'); ?></p>            
               </div>
                 <div class="col-md-3 col-sm-3" style="
    padding-bottom: 15px;" >
                     
                   <div class="about-us">تگ ها</div>
                     <div class="tags">
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                     <a href="#" class="tag-boxs"><span>طراحی</span> </a>
                        </div>
               </div>
               <div class="col-md-3 col-sm-3">
            
                   
                       <div class="about-us">اخرین اخبار</div>
                       
                    		<?php
                   
                   query_posts(array(
                       'showposts'=>2
                   
                   ));
		if ( have_posts() ) :

		

			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>


                
                   <div class="news">
                   <img src="<?php bloginfo("template_url"); ?>/assets/img/1.png" class="news-img">
                   <span class="news-contant"> <?php the_excerpt(); ?></span>
                       </div>
                   
			 <?php endwhile;
		
		endif; ?>
                   
                   
              
               </div>
                         <div class="col-md-3 col-sm-3">
                   <div class="about-us"><?php smspanel_option('newslatter'); ?></div>
                             <p class="about-us-content"><?php smspanel_option('newslattertext'); ?></p>
                             
                             <input type="text" placeholder="ایمیل خود را وارد کنید" class="footer-email">  
                             <button class="news-lett-label"><i class="fa fa-paper-plane tele-icon" aria-hidden="true"></i> </button>
               </div>

               </div>  
           </div>
       <hr  class="hr-footer">
            <div class="col-md-3 col-sm-3">
               <span class="copyright">
               <?php smspanel_option('copyright'); ?></span>
           
           </div>
           <div class="col-md-9 col-sm-9">
               <a href="#" class="footer-links">خانه</a>
               <a href="#" class="footer-links">| ویژگی ها</a>
               <a href="#" class="footer-links">| صفحات</a>
               <a href="#" class="footer-links">| درباره ما</a>
               <a href="#" class="footer-links">| تماس با ما</a>
           
           </div>
          
       </div>
       
       

  

      <script src="<?php bloginfo("template_url"); ?>/assets/js/jquery-3.2.1.min.js"></script>
      <script src="<?php bloginfo("template_url"); ?>/assets/js/bootstrap.min.js"></script> 
      <script src="<?php bloginfo("template_url"); ?>/assets/js/owl.carousel.js"></script>
      <script src="<?php bloginfo("template_url"); ?>/assets/js/owl.navigation.js"></script>
       
<script>

// ------------slide-payiiniii------------
    $(document).ready(function(){
        $('.owl-service').owlCarousel({
            nav:true,
            loop:true,
            item:3,
            margin:40,
            responsiveClass:true,
            dots:false,
            rtl:true,
            navClass: ['next-icon','previous-icon'],
            navText : ['<i class="fa fa-chevron-right" aria-hidden="true"></i>','<i class="fa fa-chevron-left" aria-hidden="true"></i>'],
            responsive:{
                0:{
                    items:4,
                    nav:true
                },
                600:{
                    items:6,
                    nav:true
                },
                1000:{
                    items:8,
                    nav:true
                }
            }
        })
    });

</script>
<?php wp_footer(); ?>
   </body>
</html>

