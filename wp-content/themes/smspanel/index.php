<?php get_header(); ?> 
      <div class="container-fluid">
       <div class="col-md-12 col-md-offset-0 col-lg-10 col-lg-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 banner">
        <div class="col-md-5 col-sm-4 col-lg-6 col-xs-7 first-page-baner">
            <div class="laptop-pic" ><img src="<?php smspanel_option( 'headerimage2' ); ?>"></div>
            <div class="mobile-pic" id="headerimage">
                <img src="<?php smspanel_option( 'headerimage' ); ?> ">
            
            
            </div>
           </div>
           <div class="col-md-3 col-sm-4 col-lg-3  col-xs-12 first-page-baner-text" >
               <h4><?php smspanel_option( 'bannertexttitle' ); ?> </h4>
               <p><?php smspanel_option( 'bannertext' ); ?></p>
           
           </div>
           

           <?php wp_nav_menu([
               'theme_location'  => '',
               'menu'            => 'Primary',
               'container'       => '',
               'container_class' => '',
               'container_id'    => '',
               'menu_class'      => '',
               'menu_id'         => '',
               'echo'            => true,
               'fallback_cb'     => 'wp_page_menu',
               'before'          => '',
               'after'           => '',
               'link_before'     => '',
               'link_after'      => '',
               'items_wrap'      => '<ul id="menu-main-menu" class="menu">%3$s</ul>',
               'depth'           => 0,
               'walker'          => ''
           ]);
           ?>

           </div>
           </div>
          <div class="clearfix"></div>
       
       <div class="container icons-box">
       
           <div class="col-md-12 col-md-offset-0 col-lg-10 col-lg-offset-1 col-sm-12 col-sm-offset-0 icons">
               
                   

                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 ">
                    <div class="icons-up">
                        <div class="border-1"></div>
                        <div class="border-2"></div>
                    <i class="fa fa-mobile" aria-hidden="true"></i>
                    <div class="icon-title"> <?php smspanel_option( 'iconshortcuttitile' ); ?></div>
                        <p><?php smspanel_option('iconshortcut'); ?></p>
                                      </div>
               </div>
                 <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                     <div class="icons-up">
                         <div class="border-1"></div>
                        <div class="border-2"></div>
                     <i class="fa fa-comment-o" aria-hidden="true"></i>
                     <div class="icon-title"><?php smspanel_option( 'iconshortcuttitile2' ); ?></div>
                          <p><?php smspanel_option('iconshortcut2'); ?></p>
                     </div>
               </div>
               <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                   <div class="icons-up">
                       <div class="border-1"></div>
                        <div class="border-2"></div>
                   <i class="fa fa-desktop" aria-hidden="true"></i>
                       <div class="icon-title"> <?php smspanel_option( 'iconshortcuttitile3' ); ?></div>
                        <p><?php smspanel_option('iconshortcut3'); ?></p>
                   </div>
               </div>
               <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                   <div class="icons-up">
                       <div class="border-1"></div>
                        <div class="border-2"></div>
                   <i class="fa fa-star-o " aria-hidden="true"></i>
                       <div class="icon-title"> <?php smspanel_option( 'iconshortcuttitile4' ); ?></div>
                        <p><?php smspanel_option('iconshortcut4'); ?></p>
                   </div>
               </div>
          </div>
           </div>
           <div class="col-md-1 nopadding back-part3">
               <div class="green-little-box">
                    <i class="fa fa-briefcase icon-brif" aria-hidden="true"></i>
               </div>
            <div class="container">
                <div class="gray-part-title">
                <div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-1">
                    <span class="title-gray"><?php smspanel_option('grayparttitle'); ?></span>
                    <span class="title-green"><?php smspanel_option('grayparttitle2'); ?></span>
                    <p><?php smspanel_option('grayparttext'); ?></p>
                </div>
                
                <div class="container col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 pd">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                        <div class="num">
                        <span><?php smspanel_option('num1'); ?></span>
                        </div>
                        <div class="line-icon">
                            
                            <i class="fa fa-check-square-o check-icon" aria-hidden="true"></i>
                  
                        </div>
                        <div class="dec-bottom">
                        <p><?php smspanel_option('num1text'); ?></p>
                        </div>
                    </div>
                           <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                        <div class="num">
                        <span><?php smspanel_option('num2'); ?></span>
                        </div>
                        <div class="line-icon">
                            <i class="fa fa-hourglass-half check-icon" aria-hidden="true"></i>
                        
                        </div>
                        <div class="dec-bottom">
                        <p><?php smspanel_option('num2text'); ?></p>
                        </div>
                    
                    
                    </div>
                           <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                        <div class="num">
                        <span><?php smspanel_option('num3'); ?></span>
                        </div>
                        <div class="line-icon">
                          <i class="fa fa-tags check-icon" aria-hidden="true"></i>
                        
                        </div>
                        <div class="dec-bottom">
                        <p><?php smspanel_option('num3text'); ?> </p>
                        </div>
                    
                    
                    </div>
                           <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                        <div class="num">
                        <span><?php smspanel_option('num4'); ?></span>
                        </div>
                        <div class="line-icon">
                        <i class="fa fa-thumbs-up check-icon" aria-hidden="true"></i>
                        
                        </div>
                        <div class="dec-bottom">
                        <p><?php smspanel_option('num4text'); ?> </p>
                        </div>
                                 
                    </div>
                </div>
            
                </div>
               </div>
       </div>
       
                <div class="col-md-12 nopadding blog-posts-section">
                    
                    
                    
                    
                    		<?php
		if ( have_posts() ) :

		

			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>

   <div class="col-md-6 col-sm-6 nopadding">
                        
                  <div class="col-md-6 col-sm-6 nopadding">
                   <div class="oragne-boxes-b">
                       <span class="catgory"><?php the_category(' ; '); ?></span>
                       <h4><?php the_title(); ?></h4>
                       <hr class="hr-class">
                       <span class="mtn">
                         <?php the_excerpt(); ?>
                       </span>
                       <a href="<?php the_permalink(); ?>" class="go-link">ادامه مطلب</a>                   
                    </div>
                   </div>
                   <div class="col-md-6 col-sm-6 nopadding img-box">
<!--                            <img src="<?php bloginfo("template_url"); ?>/assets/img/4.png">-->
                       <?php the_post_thumbnail(); ?>
                       </div>                 
                   </div>
			 <?php endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
                    
            
       
              

               </div>
       <div class="clearfix"></div>
       <div class="back-part2">
           <div class="container">
           
        <div class="owl-carousel owl-service">
            
                       <?php
$customers = new WP_Query([
    'post_type'         => 'customer',

    'order'             => 'desc',
]);
?>
      
<?php 
for ($i=0; $customers->have_posts(); $i++) :
    $customers->the_post();
    $CustomerName = get_post_meta(get_the_ID(), ' customer_name', true);
    $CustomerLink = get_post_meta(get_the_ID(), ' customer_link', true);
    $customerLogo = get_the_post_thumbnail_url();
?>

             <div class="logo-brand">
           <img src="<?=$customerLogo?>">
      

           </div>
            
<?php endfor; ?>
      
            <div class="next-icon"></div>
               <div class="previous-icon"></div>
             </div>
               
               
           
           
       </div>
       </div>




<!--
=======
for ($i=0; $banners->have_posts(); $i++) {
    $banners->the_post();
    $panelName = get_post_meta(get_the_ID(), 'panel_name', true);
    $panelPrice = get_post_meta(get_the_ID(), 'panel_price', true);
    $panelSpecial = (boolean) get_post_meta(get_the_ID(), 'panel_special', true);

    echo $panelName . '<>' . $panelPrice;
}


/** Post Type: Customer
 * customer_name
 * customer_link
 * get_the_post_thumbnail_url()
 */

?>
>>>>>>> ffebc3dcafb7e92c74ad3b893d3801270d9cf53b
-->

     
       <div class="clearfix"></div>
           <div class="container">
               <div class="price-table">
                
                <div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-1 gray-part-title">
                    <span class="title-gray-2"><?php smspanel_option('tablepricetitile'); ?> </span>
                    <span class="title-green"><?php smspanel_option('tablepricetitile2'); ?> </span>
                    <p class="para-titile"><?php smspanel_option('tablepricetext'); ?> </p>
                </div>
                   <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 col-lg-10 col-lg-offset-1 nopadding">
                       
<?php
$panels = new WP_Query([
    'post_type'         => 'panel',
    'posts_per_page'    => 5,
    'order'             => 'desc',
]);
?>
                       
<?php for ($i=0; $panels->have_posts(); $i++) :
                       $panels->the_post();
                       $panelName = get_post_meta(get_the_ID(), 'panel_name', true);
                       $panelPrice = get_post_meta(get_the_ID(), 'panel_price', true);
                       $panelSpecial = (boolean) get_post_meta(get_the_ID(), 'panel_special', true);
                       

?> 
                       

    
            <div class="col-md-3 col-sm-6 col-xs-12 col-lg-3">
                           <div class="price-up-box">
                               <?php if ($panelSpecial) :
                             echo '<i class="fa fa-star star-price" aria-hidden="true"></i>'  ?> 
                                    <?php endif; ?>
                               
                               <?=$panelName?> 
                               
                           <?php if ($panelSpecial):
                                 echo '<i class="fa fa-star star-price" aria-hidden="true"></i>' ?>
                
                               <?php endif; ?>
                </div>
                           <div class="box-of-prices">
                               <div class="price">
                                     <span class="price-limits">ماهانه</span>
                                   <span class="price-text"><?=$panelPrice?></span>
                                   <span class="price-con">تومان</span>
                               </div>
                               <div class="poan-of-service">
                                   <p>5پروژه</p>
                                   <p>5گیگ حافظه</p>
                                   <p>کاربران نامحدود</p>
                                   <p>10گیگ پهنای باند</p>
                               </div>
                               <div class="sign-in">
                               <button class="sign-btn">عضو شوید</button>
                               </div>
                           </div>
                       </div>
<?php endfor; ?>
               
<!--
             
                              <div class="col-md-3 col-sm-6 col-xs-12 col-lg-3">
                           <div class="price-up-box premium"><i class="fa fa-star star-price" aria-hidden="true"></i> مدل ویژه<i class="fa fa-star star-price" aria-hidden="true"></i> </div>
                           <div class="box-of-prices">
                               <div class="price">
                                     <span class="price-limits">ماهانه</span>
                                   <span class="price-text">19000</span>
                                   <span class="price-con">تومان</span>
                               </div>
                               <div class="poan-of-service">
                                   <p>5پروژه</p>
                                   <p>5گیگ حافظه</p>
                                   <p>کاربران نامحدود</p>
                                   <p>10گیگ پهنای باند</p>
                               </div>
                               <div class="sign-in">
                               <button class="sign-btn">عضو شوید</button>
                               </div>
                           </div>
                       </div>
                 
                       
-->
                   
                   </div>
                </div>
               </div>
       <div class="green-back">
           <div class="map">
           </div>
       </div>
       <div class="clearfix"></div>
       
       <div class="connect-part-back">
           <div class="container">
           <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
               <div class="box-of-connect-us">
                   <div class="col-md-5 col-sm-5 col-lg-5 col-xs-12">
                       <div class="col-md-12 col-sm-12  nopadding">
                           <div class="title-connect-to-us">
                              <?php smspanel_option('titleconnecttous'); ?>
                           </div>
                           <p class="content-connect-to-us">
                               <?php smspanel_option('connecttous'); ?>
                           </p>
                           <div class="location">
                               <i class="fa fa-map-marker icons-connect" aria-hidden="true"></i>
                               <p> <?php smspanel_option('location'); ?></p>
                           </div>
                             <div class="location">
                             <i class="fa fa-phone icons-connect" aria-hidden="true"></i>
                           <span><?php smspanel_option('tellnumb'); ?></span>
                           </div>
                           <div class="location">
                             <i class="fa fa-envelope-o icons-connect" aria-hidden="true"></i>
                           <span><?php smspanel_option('email'); ?></span>
                           </div>
                       </div>
                   
                   </div>
                   <div class="col-md-7 col-sm-7 col-xs-12 col-lg-7">
                       <div class="inputs-part col-md-12 col-sm-12 col-xs-12">
                       <lable>نام شما</lable><br>
                       <input type="text" class="col-md-12 col-sm-12  col-xs-12 inp username" placeholder="نام شما" alt="">
                 </div>
                       <div class="inputs-part col-md-12 col-sm-12 col-xs-12 ">
                           <lable>ایمیل شما</lable><br>
                       <input type="email" class="col-md-12 col-sm-12 col-xs-12 inp email" placeholder="ایمیل شما" alt="">
                           </div>
                           <div class="inputs-part col-md-12 col-sm-12 col-xs-12">
                               <lable>موضوع</lable><br>
                       <input type="text" class="col-md-12 col-sm-12 col-xs-12 inp subject" placeholder="موضوع" alt="">
                               </div>
                               <div class="inputs-part col-md-12 col-sm-12 col-xs-12">
                                   <lable>متن</lable><br>
                       <input type="text" class="col-md-12 col-sm-12 col-xs-12 inp message" placeholder="پیام شما " alt="">
                                   </div>
                       <div class="col-md-2 col-sm-3 col-lg-2 col-xs-4 col-xs-12 btn-snd">
                       <button class="send-btn">ارسال</button>
                       </div>     
                   </div>
               </div>
               </div>
           </div>
       </div>
       
       
       <div class="clearfix"></div>
<?php get_footer(); ?>