<?php
// custom post customer

// Register Custom Post Type
function custom_post_type_customer() {

    $labels = array(
        'name'                  => _x( 'مشتری ها', 'نام کلی هر مشتری', 'text_domain' ),
        'singular_name'         => _x( 'مشتری', 'نام هر مشتری', 'text_domain' ),
        'menu_name'             => __( 'مشتری ها', 'text_domain' ),
        'name_admin_bar'        => __( 'مشتری ها', 'text_domain' ),
        'archives'              => __( 'مشتری ها', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'همه ی مشتری ها', 'text_domain' ),
        'add_new_item'          => __( 'اضافه کردن مشتری جدید', 'text_domain' ),
        'add_new'               => __( 'افزودن', 'text_domain' ),
        'new_item'              => __( 'مشتری جدید', 'text_domain' ),
        'edit_item'             => __( 'ویرایش', 'text_domain' ),
        'update_item'           => __( 'به روز رسانی', 'text_domain' ),
        'view_item'             => __( 'مشاهده', 'text_domain' ),
        'search_items'          => __( 'جستوجو', 'text_domain' ),
        'not_found'             => __( 'پیدا نشد', 'text_domain' ),
        'not_found_in_trash'    => __( 'در زباله دان پیدا نشد', 'text_domain' ),
        'featured_image'        => __( 'تصویر', 'text_domain' ),
        'set_featured_image'    => __( 'انتخاب تصویر', 'text_domain' ),
        'remove_featured_image' => __( 'پاک کردن تصویر', 'text_domain' ),
        'use_featured_image'    => __( 'استفاده بع عنوان توصیر مشتری', 'text_domain' ),
        'insert_into_item'      => __( 'اضافه کردن به مشتری', 'text_domain' ),
        'uploaded_to_this_item' => __( 'اپلود کردن', 'text_domain' ),
        'items_list'            => __( 'لیست مشتری ها', 'text_domain' ),
        'items_list_navigation' => __( 'منوی لیست مشتری ها', 'text_domain' ),
        'filter_items_list'     => __( 'لیست مشتری ها فیلتر شده', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'مشتری ها', 'text_domain' ),
        'description'           => __( 'مشتری ها', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'rewrite'				=> array('slug' => 'customer'),
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'customer', $args );
    flush_rewrite_rules();

}
add_action( 'init', 'custom_post_type_customer', 0 );



if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_m594a5e1791dab',
        'title' => 'مشتری',
        'fields' => array (
            array (
                'key' => 'field_m594a5e210b7b1',
                'label' => 'نام مشتری',
                'name' => 'customer_name',
                'type' => 'text',
                'instructions' => 'نام مشتری را وراد کنید',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'field_type' => 'text',
                'allow_null' => 0,
                'add_term' => 0,
                'save_terms' => 1,
                'load_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
            ),
            array (
                'key' => 'field_m594a5e210b7b8',
                'label' => 'لینک به مشتری',
                'name' => 'customer_link',
                'type' => 'text',
                'instructions' => 'قیمت مشتری را وارد کنید',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'field_type' => 'text',
                'allow_null' => 0,
                'add_term' => 0,
                'save_terms' => 1,
                'load_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
            ),
            array (
                'key' => 'field_m594a5e210b7b8',
                'label' => 'لینک به مشتری',
                'name' => 'customer_link',
                'type' => 'text',
                'instructions' => 'قیمت مشتری را وارد کنید',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'field_type' => 'text',
                'allow_null' => 0,
                'add_term' => 0,
                'save_terms' => 1,
                'load_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'customer',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'acf_after_title',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array (
            0 => 'permalink',
            1 => 'the_content',
            2 => 'excerpt',
            3 => 'discussion',
            4 => 'comments',
            5 => 'revisions',
            6 => 'slug',
            7 => 'author',
            8 => 'format',
            9 => 'page_attributes',
            10 => 'categories',
            11 => 'tags',
            12 => 'send-trackbacks',
        ),
        'active' => 1,
        'description' => '',
    ));

endif;



?>