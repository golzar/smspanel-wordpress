<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package smspanel
 */

get_header(); ?>

       

		<?php
		while ( have_posts() ) : the_post();

?>




       <div class="banner-img">
           
        <div class="header-img" style="background-image: url('<?=get_the_post_thumbnail_url(); ?>')">  </div>
        <span><?php the_title(); ?></span>
       </div>

       <div class="back-color">
           <div class="conatiner">
               <div class="col-md-8 col-md-offset-2 col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                   <div class="center-box">
                       <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 img-box pull-right">
                       <img src= <?php the_post_thumbnail(); ?> 
                       </div>

                       <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 category">
                           <?php $tags = wp_get_post_tags(get_the_ID()); ?>
                           <?php foreach ($tags as $t) : ?>
                           <a href="<?php $tags = wp_get_post_tags(get_the_ID()); ?>" class="category-1">  <?=$t->name?> </a>
                 <?php endforeach; ?>
                           
                           


                       <div class="date-of-post">
                       <span><?php the_date(); ?></span>
                       </div>
                             </div>
                       <div class="content-post-box">
                           <p><?php the_content(); ?></p>
                       
                       
                       </div>
                   </div>
       
               </div>
                 </div>
                </div>
</div>

<?php
		endwhile; // End of the loop.
		?>

       <div class="posts">
           <div class="clearfix"></div>
<?php
   query_posts(array(
                       'showposts'=>4
                   
                   ));
		while ( have_posts() ) : the_post();
?>

               <div class="col-md-3 col-sm-4 col-xs-6 col-lg-3">
                   <div class="box-of-button-post">
                       <div class="img-posts-1">
                       <img src=<?php the_post_thumbnail(); ?> 
                           </div>
                       <h5><?php the_title(); ?></h5>
                       <p> <?php the_excerpt(); ?></p>
                       <a href="<?php the_permalink(); ?>">ادامه مطلب...</a>
                   
                   </div>
               </div>
     


  
		<?php


		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer(); ?>
