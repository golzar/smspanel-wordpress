<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smspanel
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
   <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
    
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/style.css">
      <link rel="apple-touch-icon" href="apple-touch-icon.png">
      <!-- Place favicon.ico in the root directory -->
      <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/normalize.css">
      <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/main.css">
      <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/assets/css/font-awesome.css">
      <script src="<?php bloginfo("template_url"); ?>/assets/js/vendor/modernizr-2.8.3.min.js"></script>
       <?php wp_head(); ?>
   </head>
    
   <body <?php body_class(); ?>>
      <nav class="navbar navbar-default nav-bg">
         <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <span class="nav navbar-nav navbar-left" href="#">
                   پنل پیامک        
                </span>
            </div>
<!--
               <ul class="nav navbar-nav navbar-link navbar-brand brand-logo pull-right ">
                  <li class="logo">
                     <img src="">
                     
                  </li>
               </ul>
-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right">
                 <li><a href="#">خانه</a></li> 
                 <li><a href="#">ویژگی ها</a></li> 
                 <li><a href="#">صفحات</a></li> 
                 <li><a href="#">درباره ما</a></li> 
                 <li><a href="#">ارتباط با ما</a></li> 
               </ul>
             
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
       



















